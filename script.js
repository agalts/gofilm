APIkey = "7bc1893f"

function SearchFilm(name)
{
    let FilmRequest = 'http://www.omdbapi.com/?apikey='+APIkey+'&t='+name;

    let request;
    if(window.XMLHttpRequest)
        request = new XMLHttpRequest();
    else
        request = new ActiveXObject("Microsoft.XMLHTTP");

    request.open("GET", FilmRequest);
    request.onload = function()
    {
        if(request.status === 200)
            setTimeout(function(){document.getElementById("filmAnswer").innerHTML = request.response;},100);
    }
    
    setTimeout(function(){request.send();},100);

    setTimeout(function(){

        let _info = JSON.parse(document.getElementById("filmAnswer").innerHTML);

        if (typeof _info.Title === 'undefined') {
            window.location.href="404.html";
           return 0;
        }

        document.getElementById("FilmInfo").style = "display:block;"
        document.getElementById("f_title").innerHTML = _info.Title;
        document.getElementById("f_out").innerHTML = _info.Released;
        document.getElementById("f_lang").innerHTML = _info.Language;
        document.getElementById("f_genre").innerHTML = _info.Genre;
        document.getElementById("f_country").innerHTML = _info.Country;
        document.getElementById("f_director").innerHTML = _info.Director;
        document.getElementById("f_writer").innerHTML = _info.Writer;
        document.getElementById("f_actors").innerHTML = _info.Actors;
        document.getElementById("f_awards").innerHTML = _info.Awards;
        document.getElementById("poster").src = _info.Poster;

        let imdb;let metascore;let tomato;

        try{
            imdb = _info.imdbRating;
            metascore = _info.Metascore;
            tomato = _info.Ratings[1].Value;
        }
        catch{}

        document.getElementById("RTG").style = "display:block;";
        document.getElementById("r_imdb").innerHTML = imdb + '/10 (' + _info.imdbVotes + ')';
        document.getElementById("r_metascore").innerHTML = _info.Metascore;
        document.getElementById("r_tomato").innerHTML = tomato;
        
        if (imdb == "N/A")
            document.getElementById("r_imdb").innerHTML = "Нет оценки";
        if (metascore == "N/A")
            document.getElementById("r_metascore").innerHTML = "Нет оценки";
            
        if (typeof tomato === 'undefined') {
            document.getElementById("r_tomato").innerHTML = "Нет оценки";
        }

        document.getElementsByClassName("form")[0].style = "border-color:green;";
    },1000);
}